﻿using UnityEngine;

public class PlayerSelect : MonoBehaviour {

    public string playerName; // character name we will use
    MainMenuManager mmm; // Reference to our GameManager object
	
    // Use this for initialization
	void Start () {
        mmm = GameObject.Find("MainMenuManager").GetComponent<MainMenuManager>(); // Get a reference to our GameManager object
	}
	
	// Update is called once per frame
	void Update () {

    }

    // Function to detect if the player clicked our collider
    private void OnMouseUp()
    {
        mmm.SelectCharacter(playerName);
    }
}
