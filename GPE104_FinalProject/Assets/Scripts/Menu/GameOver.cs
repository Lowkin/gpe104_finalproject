﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Function to handle returning to the main menu
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0); // Return to main menu
    }

    // Function to handle exiting
    public void Exit()
    {
        Application.Quit(); // Exit
    }
}
