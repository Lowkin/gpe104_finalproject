﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    private float rate;
	// Use this for initialization
	void Start () {
        rate = Random.Range(50, 101);
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.forward, Time.deltaTime * rate);
	}
}
