﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobUpAndDown : MonoBehaviour {

    private float originalY; // Variable to store our original Y value

    public float distance = .5f;
    public float speed = 1f;

	// Use this for initialization
	void Start () {
        this.originalY = this.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        // bob our position up and down
        transform.position = new Vector3(transform.position.x,
            originalY + ((float)Mathf.Sin(Time.time * speed) * distance),
            transform.position.z);
    }
}
