﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public static LevelManager lm; // Singleton GameManager object

    public bool gameOver; // Variable to hold our game over status

    public float cameraYBorder; // Variable to hold how low our camera can go

    public string nextLevel; // Variable to hold the name of the next scene

    public PlayerController playerController; // Variable to hold a reference to our player controller

    public Camera mainCamera; // Variable to hold a reference to our main camera
    public GameObject cameraLeftBound; // Variable to hold the left most our camera is allowed to go
    public GameObject cameraRightBound; // Variable to hold the right most our camera is allowed to go

    public GameObject playerPawn; // reference to our player's current pawn
    public GameObject playerSpawnPoint; // reference to where we spawn the player

    public Sprite[] uiDigits; // Array to hold references to our UI numbers for display

    /* Variables to hold the player prefabs for instantiation */
    public GameObject player1Prefab;
    public GameObject player2Prefab;
    public GameObject player3Prefab;

    /* Variables to hold the player icon images */
    public Sprite player1Icon;
    public Sprite player2Icon;
    public Sprite player3Icon;

    /* Variable to hold references to our score digits */
    public Image[] scoreDigits;

    public Image playerIcon; // Variable to hold reference to our player icon
    public Image lifeDigit; // Variable to hold reference to our player life digit

    public GameObject pauseMenu; // Variable to hold a reference to our Pause menu
    public GameObject winMenu; // Variable to hold a reference to our Win menu

    public AudioSource cameraAudioSource; // Variable to hold our camera audio source
    public AudioSource secondaryCameraSource; // Variable to hold a secondary audio source at our camera

    public AudioClip coinCollectionSound; // Variable to hold our coin collection sound
    public AudioClip loseLifeSound; // Variable to hold our life lost sound
    public AudioClip killEnemySound; // Variable to hold our kill enemy sound
    public AudioClip extraLifeSound; // Variable to hold our extra life sound
    public AudioClip victoryFanfare; // Variable to hold our victory sound
    public AudioClip jumpSound; // Variable to hold our jump sound

    [HideInInspector]
    public enum States { Active, Paused, GameOver, Victory }; // Variable to hold the various states our game
    [HideInInspector]
    public States currentState; // Variable to hold the current state of our pawn

    private GameManager gm; // Reference to our GameManager object
    private bool pauseAxisInUse = false; // Variable to tell if we are already holding the pause button down
    private bool gamePaused = false; // Variable to hold the paused state of our game

    void Awake () {

        // If we don't have a GameManager instance set our singleton to this instance
        if (lm == null)
            lm = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);

        gm = GameObject.Find("GameManager").GetComponent<GameManager>(); // Get a reference to our GameManager Object
    }

    // Use this for initialization
    void Start()
    {
        currentState = States.Active;  // Start the level in an Active state

        if (playerPawn == null)
            SpawnPlayer();

        UpdateLives(0); // update our life total UI

        mainCamera.transform.position = new Vector3(playerPawn.transform.position.x, playerPawn.transform.position.y, mainCamera.transform.position.z); // set our camera to our player

        StartCoroutine("LevelFSM");
    }

    // Update is called once per frame
    void Update () {

        // Hack to skip levels
        if (Input.GetKeyDown(KeyCode.P))
            GoToNextLevel();

        float pauseInput = Input.GetAxisRaw("Esc");
        // If we hit the escape key (or start button on controller) we pause the game
        if (pauseInput != 0)
        {
            // if we are not already holding the button down we set that we are and we pause
            if (!pauseAxisInUse)
            {
                pauseAxisInUse = true;
                gamePaused = !gamePaused;
            }
        }
        // if we are not holding the button down we set the inUse function to false
        else if (pauseInput == 0)
            pauseAxisInUse = false;
    }

    // Function to manage our level state machine
    public IEnumerator LevelFSM()
    {
        // We always want to be running this FMS
        while (true)
        {
            // If we are in the Active state
            if (currentState == States.Active)
            {
                pauseMenu.SetActive(false); // Set our pause menu inactive
                Time.timeScale = 1; // Unpause our timescale

                // If we have pressed the pause button
                if (gamePaused)
                    currentState = States.Paused; // set our state to pause
                else if (gameOver)
                    currentState = States.GameOver; // set our state to game over

                yield return 0;
            }
            
            else if (currentState == States.Paused)
            {
                playerController.axisInUse = true; // disable character input when selecting menu items
                pauseMenu.SetActive(true); // Set our pause menu active

                // Set our timescale to 0 so all of our movement and animations that are controlled by time will be paused
                Time.timeScale = 0;

                // If we have unpaused
                if (!gamePaused)
                    currentState = States.Active; // unpause our gamestate

                yield return 0;
            }
            // If we are in a game over state
            else if (currentState == States.GameOver)
            {
                playerController.axisInUse = true; // disable character input when selecting menu items
                pauseMenu.SetActive(false); // Set our pause menu inactive
                UpdateLives(-1); // update our lives
                AudioSource.PlayClipAtPoint(loseLifeSound, Camera.main.transform.position);
                yield return new WaitForSeconds(gm.respawnTime); // wait specified respawn time

                // Spawn our player if we have lives left
                if (gm.currentLives >= 1)
                {
                    SpawnPlayer();
                    gameOver = false;
                    currentState = States.Active;
                }
                else
                    SceneManager.LoadScene("GameOver"); // Load our game over scene
                yield return 0;
            }
            // If we have on
            else if (currentState == States.Victory)
            {
                playerController.axisInUse = true; // disable character input when selecting menu items
                Time.timeScale = 0;
                yield return 0;
            }



            yield return 0;
        }
    }

    // FUnction to update our life UI
    public void UpdateLives(int value)
    {
        gm.currentLives += value;

        // we dont want more than 9 lives
        if (gm.currentLives > 9)
            gm.currentLives = 9;

        lifeDigit.sprite = uiDigits[gm.currentLives]; // Update our life sprite
        
        // if we are at 0 lives its game over
        if (gm.currentLives == 0)
            currentState = States.GameOver;

        // if we gained a life, play sound
        if (value > 0)
            AudioSource.PlayClipAtPoint(extraLifeSound, Camera.main.transform.position);
    }

    // Function to update our score count and UI
    public void UpdateScore(int value)
    {
        // Play our coin collection sound
        secondaryCameraSource.clip = coinCollectionSound;
        secondaryCameraSource.Play();
        //AudioSource.PlayClipAtPoint(coinCollectionSound, Camera.main.transform.position);

        gm.currentScore += value; // Increment our value
        string score = "000"; // our score string

        // If we are under 10 we are a single digit score
        if (gm.currentScore < 10)
            score = "00" + gm.currentScore.ToString();

        // If we are under 100 we are a double digit score
        else if (gm.currentScore < 100)
            score = "0" + gm.currentScore.ToString();

        // Otherwise we are a 3 digit score
        else
            score = gm.currentScore.ToString();

        char[] scoreChar = score.ToCharArray(); // Get a char array to loop through our score

        // for each character in our score set the appropriate digit sprite
        for (int i = 0; i < scoreChar.Length; i++)
        {
            scoreDigits[i].sprite = uiDigits[int.Parse(scoreChar[i].ToString())];
        }
    }

    // Function to unpause the game
    public void ResumePlay()
    {
        gamePaused = false; // Set our game paused variable to false
    }

    // Function to Return to Main Menu
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0); // Load our Main Menu scene
    }

    // Function to Exit the game
    public void Exit()
    {
        Application.Quit();
    }

    // Set our camera position in late update so we know all calculations and positions have been updated
    public void LateUpdate()
    {
        // Set our camera to lerp to follow the player;  we lerp to avoid jagged movement
        if (lm.currentState == States.Active)
        {
            Vector3 newLocation = new Vector3(playerPawn.transform.position.x, playerPawn.transform.position.y, mainCamera.transform.position.z);
            if (newLocation.x < cameraLeftBound.transform.position.x)
                newLocation.x = cameraLeftBound.transform.position.x;
            else if (newLocation.x > cameraRightBound.transform.position.x)
                newLocation.x = cameraRightBound.transform.position.x;
            if (newLocation.y < cameraYBorder)
                newLocation.y = cameraYBorder;
            // If we are far away we lerp back to the spawn point
            if (Vector3.Distance(mainCamera.transform.position, newLocation) > .05f)
                mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, newLocation, Time.deltaTime * 8);
            // otherwise stay right with the player
            else
                mainCamera.transform.position = newLocation;
        }
    }

    // Function to spawn our player
    public void SpawnPlayer()
    {
        // if we have selected Player 1
        if (gm.selectedPlayer == "Player1")
            playerPawn = Instantiate(player1Prefab, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
        // if we have selected Player 2
        else if (gm.selectedPlayer == "Player2")
            playerPawn = Instantiate(player2Prefab, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
        // if we have selected Player 3
        else if (gm.selectedPlayer == "Player3")
            playerPawn = Instantiate(player3Prefab, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
    }

    // set our spawn point to the last checkpoint
    public void UpdateSpawnPoint(GameObject newLocation)
    {
        playerSpawnPoint = newLocation;
    }

    // Function to load the next level
    public void GoToNextLevel()
    {
        // if we just won the game
        if (nextLevel == "Victory")
        {
            currentState = States.Victory; // set our state to victory
            cameraAudioSource.Stop(); // stop our music
            cameraAudioSource.loop = false; // Make sure we dont loop our fanfare
            cameraAudioSource.clip = victoryFanfare; // set our camera to play victory
            cameraAudioSource.Play(); // play the victory fanfare
            winMenu.SetActive(true); // set our victory UI to active
        }
        // otherwise onto the next level
        else
            SceneManager.LoadScene(nextLevel);
    }
    // Function to load credits
    public void Credits()
    {
        SceneManager.LoadScene("Credits"); // load credits scene
    }
}
