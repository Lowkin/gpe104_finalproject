﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

    public static MainMenuManager mmm; // Singleton GameManager object

    public GameManager gm; // Reference to our GM object

    public bool boolEnableControllerButtons; // Boolean to control if we display our controller buttons

    public float yBase; // The base value of our characters when they arent selected
    public float yOffset; // THe offset we move our selected character sprite when its been scaled

    public string selectedPlayer; // String to hold our currently selected player name

    /* Strings to reference our controller buttons */
    public string xBoxYButton;
    public string xBoxAButton;
    public string xBoxBButton;
    public string xBoxXButton;

    public Vector3 selectedScale; // Vector to control the scale of our selected character

    /* Sprite references for our player selections */
    public SpriteRenderer player1;
    public Sprite player1Selected;
    public Sprite player1UnSelected;
    public SpriteRenderer player2;
    public Sprite player2Selected;
    public Sprite player2UnSelected;
    public SpriteRenderer player3; // 
    public Sprite player3Selected;
    public Sprite player3UnSelected;

    /* GameObject references for our player icons */
    public Sprite player1Icon;
    public Sprite player2Icon;
    public Sprite player3Icon;
    public SpriteRenderer playerIcon;

    public RectTransform playRect; // Our Play! text reference 

    /* GameObjects of our controller buttons */
    public GameObject x;
    public GameObject y;
    public GameObject b;
    public GameObject a;

    public GameObject playButton; // Reference to our play button

    public GameObject selectionStar; // Star used to indicate the selected character

    public AudioClip menuSelection; // Variable to hold our menu selection sound
    public AudioSource cameraAudioSource; // Variable to hold our camera audio source

    private void Awake()
    {
        // If we don't have a GameManager instance set our singleton to this instance
        if (mmm == null)
            mmm = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>(); // Get a reference to our game manager
        selectedPlayer = "Player1"; // By default we will use the Player1 player
        SelectCharacter(selectedPlayer);
        StartCoroutine(CheckForController()); // Start our controller detection
        Time.timeScale = 1; //Set our timescale to 1 in case we are returning from a paused state
	}
	
	// Update is called once per frame
	void Update () {

        EnableControllerIcons(boolEnableControllerButtons); // enable or disable controller support

        // If we have our controller plugged in check for input
        if (boolEnableControllerButtons)
        {
            if (Input.GetButtonUp(xBoxXButton))
                SelectCharacter("Player1");
            else if (Input.GetButtonUp(xBoxYButton))
                SelectCharacter("Player2");
            else if (Input.GetButtonUp(xBoxBButton))
                SelectCharacter("Player3");
            //else if (Input.GetButtonUp(xBoxAButton))
                //Play();
        }

    }

    // Function to enable/disable controller button display
    private void EnableControllerIcons(bool toggle)
    {
        /* Set our controller images active */
        y.SetActive(toggle);
        x.SetActive(toggle);
        b.SetActive(toggle);

        /*disable/enable our selection star*/
        selectionStar.gameObject.SetActive(!toggle);

        // If we have unplugged our controller and plugged it back in we need to make sure we have a selected button for navigation
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == null)
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(playButton);
    }

    // Function to set the current selected character and various UI elements relating to it
    public void SelectCharacter(string playerName)
    {
        /*Play our menu item clip */
        cameraAudioSource.clip = menuSelection;
        cameraAudioSource.Play();

        // We set our current selected player
        gm.selectedPlayer = playerName;

        // We switch depending on which player is selected
        switch (gm.selectedPlayer)
        {
            case "Player1":
                DeselectCharacters(); // Reset all characters
                player1.gameObject.transform.localScale = selectedScale; // Up our GameObjects scale
                player1.gameObject.transform.position = new Vector3(player1.gameObject.transform.position.x, yBase + yOffset, 0f); // shift our larger character up a bit
                selectionStar.transform.position = new Vector3(player1.transform.position.x, selectionStar.transform.position.y, 0f); // shift our selection star to the player
                playerIcon.sprite = player1Icon; // Set our Player icon to Player 1
                playerIcon.gameObject.SetActive(true); // Set Player 1 Icon active
                player1.sprite = player1Selected; // Set our Player 1 Sprite to the selected sprite
                break;
            case "Player2":
                DeselectCharacters(); // Reset all characters
                player2.gameObject.transform.localScale = selectedScale; // Up our GameObjects scale
                player2.gameObject.transform.position = new Vector3(player2.gameObject.transform.position.x, yBase + yOffset, 0f); // shift our larger character up a bit
                selectionStar.transform.position = new Vector3(player2.transform.position.x, selectionStar.transform.position.y, 0f); // shift our selection star to the player
                playerIcon.sprite = player2Icon; // Set our Player icon to Player 2
                playerIcon.gameObject.SetActive(true); // Set Player 2 Icon active
                player2.sprite = player2Selected; // Set our Player 2 Sprite to the selected sprite
                break;
            case "Player3":
                DeselectCharacters(); // Reset all characters
                player3.gameObject.transform.localScale = selectedScale; // Up our GameObjects scale
                player3.gameObject.transform.position = new Vector3(player3.gameObject.transform.position.x, yBase + yOffset, 0f); // shift our larger character up a bit
                selectionStar.transform.position = new Vector3(player3.transform.position.x, selectionStar.transform.position.y, 0f); // shift our selection star to the player
                playerIcon.sprite = player3Icon; // Set our Player icon to Player 3
                playerIcon.gameObject.SetActive(true); // Set Player 3 Icon active
                player3.sprite = player3Selected; // Set our Player 3 Sprite to the selected sprite
                break;
            default:
                break;
        }
    }

    // Function to deselect all characters
    private void DeselectCharacters()
    {
        /* Reset our character sprites to unselected */
        player1.sprite = player1UnSelected;
        player2.sprite = player2UnSelected;
        player3.sprite = player3UnSelected;

        /* Reset our scale */
        player1.gameObject.transform.localScale = Vector3.one;
        player2.gameObject.transform.localScale = Vector3.one;
        player3.gameObject.transform.localScale = Vector3.one;

        /* Reset our position */
        player1.gameObject.transform.position = new Vector3(player1.gameObject.transform.position.x, yBase, 0f);
        player2.gameObject.transform.position = new Vector3(player2.gameObject.transform.position.x, yBase, 0f);
        player3.gameObject.transform.position = new Vector3(player3.gameObject.transform.position.x, yBase, 0f);

        /* Deactivate all of our icons */
        playerIcon.gameObject.SetActive(false);
    }

    // Function to determine if we have a controller connected
    private IEnumerator CheckForController()
    {
        string[] controllers = Input.GetJoystickNames(); // Get all of our joysticks
        bool temp = false; // our connection boolean

        // If we have joysticks connected
        if (controllers.Length > 0)
        {
            // Loop through the joysticks connected
            for (int i = 0; i < controllers.Length; i++)
            {
                // If we find one that contains 'Xbox' in its name we set our boolean to true
                if (controllers[i].Contains("Xbox"))
                {
                    temp = true;
                    break;
                }
            }
            boolEnableControllerButtons = temp; // Set our controller boolean
        }
        yield return new WaitForSeconds(2f); // Only run this every couple seconds
        StopAllCoroutines(); // Stop the coroutine
        StartCoroutine(CheckForController()); // Start our coroutine again
    }

    // Function to exit
    public void Exit()
    {
        Application.Quit(); // Exit the application
    }

    // Function to start the game
    public void Play()
    {
        gm.currentLives = gm.startingLives; // Set our lives back to base
        gm.currentScore = 0; // Set our score back to base
        SceneManager.LoadScene("LevelOne"); // Load our first scene
    }

    // Function to go to our credits menu
    public void Credits()
    {
        SceneManager.LoadScene("Credits"); // Load our credits scene
    }
}
