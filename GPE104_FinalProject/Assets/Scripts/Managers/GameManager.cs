﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager gm; // Singleton GameManager object

    public float maxJumps; // how many jumps can we do before landing again
    public int startingLives = 3; // how many lives does the player start with
    public int currentLives = 3; // how many lives does the player currently have
    public int currentScore = 0; // our players current score
    public float respawnTime = 1.5f; // how long we wait to respawn the player

    // declarations for our various layers
    [HideInInspector]
    public int playerLayer;
    [HideInInspector]
    public int enemyLayer;
    [HideInInspector]
    public int collectibleLayer;
    [HideInInspector]
    public int groundLayer;
    [HideInInspector]
    public int checkPointLayer;
    [HideInInspector]
    public int environmentLayer;
    [HideInInspector]
    public int outOfBoundsLayer;
    [HideInInspector]
    public int exitLevelLayer;
    [HideInInspector]
    public int wallLayer;
    [HideInInspector]
    public int collectorLayer;

    public string selectedPlayer;

    // Use this for initialization
    void Start () {
        // If we don't have a GameManager instance set our singleton to this instance
        if (gm == null)
            gm = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

        // Assign our layer variables
        playerLayer = LayerMask.NameToLayer("Player");
        enemyLayer = LayerMask.NameToLayer("Enemy");
        collectibleLayer = LayerMask.NameToLayer("Collectible");
        groundLayer = LayerMask.NameToLayer("Ground");
        checkPointLayer = LayerMask.NameToLayer("CheckPoint");
        environmentLayer = LayerMask.NameToLayer("Environment");
        outOfBoundsLayer = LayerMask.NameToLayer("OutOfBounds");
        exitLevelLayer = LayerMask.NameToLayer("ExitLevel");
        wallLayer = LayerMask.NameToLayer("Wall");
        collectorLayer = LayerMask.NameToLayer("Collector");

        selectedPlayer = "Player1"; // Start off with a default selected player.

        Physics2D.IgnoreLayerCollision(9, 17); // Ignore collisions between enemies and the collector collider
        Physics2D.IgnoreLayerCollision(9, 9); // Ignore collisions bewteen enemies

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
