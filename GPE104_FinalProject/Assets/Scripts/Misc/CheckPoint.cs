﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    [HideInInspector]
    public GameManager gm; // gm reference
    [HideInInspector]
    public LevelManager lm; // gm reference

    public Sprite activeSprite; // reference for our activated look
    public AudioClip trigger; // reference for trigger sound

    private SpriteRenderer sr; // sprite renderer reference
    private BoxCollider2D bc; // capsule collider reference
    
    // Use this for initialization
    void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager; // set our game manager reference
        lm = GameObject.Find("LevelManager").GetComponent<LevelManager>() as LevelManager; // set our game manager reference
        sr = GetComponent<SpriteRenderer>() as SpriteRenderer; // set our sprite renderer
        bc = GetComponent<BoxCollider2D>() as BoxCollider2D; // our capsule collider reference
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // if the player enters our trigger save the new spawn point
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == gm.playerLayer)
        {
            lm.UpdateSpawnPoint(gameObject); // update our spawn location to this checkpoint

            sr.sprite = activeSprite; // set our activated sprite
            bc.enabled = false; // disable our collider as we do not need to be re-activated

            AudioSource.PlayClipAtPoint(trigger, Camera.main.transform.position);
        }
    }
}
