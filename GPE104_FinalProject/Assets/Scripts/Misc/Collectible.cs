﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    [HideInInspector]
    public LevelManager lm; // Level Manager reference
    [HideInInspector]
    public GameManager gm; // Game Manager reference

    public bool extraLife = false;

	// Use this for initialization
	void Start () {
        lm = GameObject.Find("LevelManager").GetComponent<LevelManager>() as LevelManager;
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager;
    }
	
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // If we were hit by a player
        if (collision.gameObject.layer == gm.collectorLayer)
        {
            if (!extraLife)
                lm.UpdateScore(1); // let the Level manager know we got a coin
            else
                lm.UpdateLives(1); // let the level manager know we got a extra life

            Destroy(this.gameObject); // Destroy ourselves
        }
    }
}
