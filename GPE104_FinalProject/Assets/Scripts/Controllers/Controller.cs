﻿using UnityEngine;

public abstract class Controller : MonoBehaviour {

    [HideInInspector]
    public Pawn currentPawn; // current pawn reference
    [HideInInspector]
    public LevelManager lm; // lm reference
    [HideInInspector]
    public GameManager gm; // gm reference

    // Use this for initialization
    public virtual void Start () {
        lm = GameObject.Find("LevelManager").GetComponent<LevelManager>() as LevelManager; // set our level manager reference
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager; // set our game manager reference
        currentPawn = transform.GetComponentInChildren<Pawn>() as Pawn;
    }
}
