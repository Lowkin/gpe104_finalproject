﻿using UnityEngine;

public class PlayerController : Controller {

    [HideInInspector]
    public bool axisInUse; // Variable to tell if we are already holding the jump button down

    // Use this for initialization
    public override void Start () {
        base.Start();
        axisInUse = false;
    }
	
	// Update is called once per frame
	void FixedUpdate() {

        // if we are in an active game state
        if (lm.currentState == LevelManager.States.Active && currentPawn != null)
        {
            float move = Input.GetAxis("Horizontal");
            currentPawn.Move(move);
        }
    }

    void Update()
    {
        // only process input if we are active
        if (lm.currentState == LevelManager.States.Active)
        {
            float input = Input.GetAxisRaw("Jump"); // get our jump input
            if (input != 0)
            {
                // if we are not already holding the button down we set that we are and we jump
                if (!axisInUse)
                {
                    axisInUse = true;
                    currentPawn.Jump();
                }
            }
            // if we are not holding the button down we set the inUse function to false
            else if (input == 0)
                axisInUse = false;
        }
    }
}
