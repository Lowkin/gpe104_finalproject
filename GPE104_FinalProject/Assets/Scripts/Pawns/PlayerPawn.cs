﻿using UnityEngine;

public class PlayerPawn : Pawn {

    public bool isGrounded = false; // Variable to hold if we are grounded
    public Transform groundCheck; // Variable to hold our gameobject for ground check calculations
    public float groundRadius; // Variable to hold the radius of our ground check sphere
    public LayerMask ground; // Variable to hold the layer we check against for being grounded

    private int jumpCount = 0; // Variable to hold our current jump count

    // Use this for initialization
    public override void Start () {
        base.Start();
        lm.playerController.currentPawn = this; // get a reference to our controller when we are instantiated
	}

    void FixedUpdate()
    {
        //isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, ground);  // Find out if we are standing on ground
        animator.SetBool("Grounded", isGrounded); // Set our animation boolean
        animator.SetFloat("vSpeed", rb2d.velocity.y); // Set our current y velocity
    }

    public override void Jump()
    {
        if (jumpCount < gm.maxJumps || isGrounded)
        {            
            base.Jump();
            // If we are grounded set our jump count to 0
            if (isGrounded)
                jumpCount = 0;

            animator.SetBool("Grounded", false); // Set our animator bool to false
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0f); // set our upward velocity to 0
            rb2d.AddForce(new Vector2(0, jumpForce)); // add our jump force
            jumpCount++; // increment our jump counter
            AudioSource.PlayClipAtPoint(lm.jumpSound, new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z / 2)); // play our jump sound
        }
    }

    // function to handle collisions
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == gm.groundLayer)
        {
            animator.SetBool("Grounded", true);
            isGrounded = true;
            jumpCount = 0;
        }
        // here we deal with running into the enemy or falling out of bounds
        else if (collision.gameObject.layer == gm.enemyLayer)
        {
            // Get a reference to our enemy pawn
            EnemyPawn ep = collision.gameObject.GetComponent<EnemyPawn>();

            // if we actually have an enemy pawn do more checks; otherwise we just die
            if (ep != null)
            {
                // find out if that enemy is below us
                if (collision.gameObject.transform.position.y < groundCheck.transform.position.y)
                {
                    // Play death/bounce sound
                    lm.secondaryCameraSource.clip = lm.killEnemySound;
                    lm.secondaryCameraSource.Play();

                    rb2d.velocity = new Vector2(rb2d.velocity.x, 0f); // set our upward velocity to 0
                    // if this enemy can also be killed                    
                    if (ep.canKill)
                    {
                        // kill the enemy 
                        ep.animator.SetBool("Hit", true); // Set the animation trigger
                        ep.Die();
                        // If we are holding down jump boost us by our jump force
                        if (Input.GetAxisRaw("Jump") > 0)
                            rb2d.AddForce(new Vector2(0, jumpForce)); // add our jump force to the player
                        // otherwise just a small boost
                        else
                            rb2d.AddForce(new Vector2(0, jumpForce / 5)); // add a small boost to the player
                    }
                    // else we bounce off of them regardless of if we are holding jump
                    else
                        rb2d.AddForce(new Vector2(0, jumpForce)); // add a small boost to the player

                    return; // we are done with this collision
                }

            }
            // we get here if we hit an enemy from below or in front, jumped on an enemy we cant kill, or fell off the world
            animator.SetBool("Hit", true); // Set our animation trigger
            lm.gameOver = true;
            Die();

        }
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.layer == gm.groundLayer)
        {
            Vector2 collisionPoint = collision.GetContact(0).point; // where did we hit the ground
            // if we are actually above the ground we are colliding with
            if (collisionPoint.y < groundCheck.transform.position.y)
            {
                animator.SetBool("Grounded", true);
                isGrounded = true;
            }
        }
    }

    // Function to handle a collision ending
    public void OnCollisionExit2D(Collision2D collision)
    {
        // If we are not on the ground we set our variables
        if (collision.gameObject.layer == gm.groundLayer)
        {
            animator.SetBool("Grounded", false);
            isGrounded = false;
        }
    }

    // fuction to handle entering triggers
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // here we deal with reaching the end of the level
        if (collision.gameObject.layer == gm.exitLevelLayer)
        {
            lm.GoToNextLevel(); // Go to next level
        }
        // here we deal with hitting an out of bounds collider
        if (collision.gameObject.layer == gm.outOfBoundsLayer)
        {
            lm.gameOver = true;
            Die();
        }
    }
}
