﻿using UnityEngine;

public class EnemyPawn : Pawn
{
    public bool canKill; // can this pawn be killed
    public bool isMoving; // is the pawn moving;
    public bool lockHorizontalMovement; // bool to control if this pawn moves horizontally or not

    private void FixedUpdate()
    {
        // if we are still alive we dont want any velocity
        if (alive)
            rb2d.velocity = Vector2.zero;
    }
}
