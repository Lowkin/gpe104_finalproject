﻿using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    public float pawnSpeed;  // float to control this pawns speed
    public float jumpForce; // float to control this pawns jump force
    public float deathForce; // float to control how much force is applied to this character on death
    public float deathTime; // float to control how long we wait till destroying the pawn
    public bool alive; // boolean to tell if the pawn is alive

    public SpriteRenderer sr; // our sprite renderer reference

    public Animator animator; // animator reference

    public Collider2D[] colliders; // Variable to hold all colliders we will need to disable upon death

    [HideInInspector]
    public GameManager gm; // gm reference

    [HideInInspector]
    public LevelManager lm; // lm reference

    [HideInInspector]
    public Rigidbody2D rb2d; // Variable to hold our rigidbody2d

    public bool facingRight;

    // Use this for initialization
    public virtual void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager; // get a reference to our game manager
        lm = GameObject.Find("LevelManager").GetComponent<LevelManager>() as LevelManager; // get a reference to our game manager
        rb2d = GetComponent<Rigidbody2D>() as Rigidbody2D; // Get our rigidbody reference
        alive = true;  // we start alive
	}
	
	// Update is called once per frame
	public virtual void Update () {
        
    }

    // Base function to move this pawn via physics
    public virtual void Move(float speed)
    {
        // Get our speed in absolute values
        animator.SetFloat("Speed", Mathf.Abs(speed));

        // Set our new speed for our rigid body
        rb2d.velocity = new Vector2(speed * pawnSpeed, rb2d.velocity.y);

        // Set our sprite orientation based on that speed
        if (speed > 0 && !facingRight)
            FlipOrientation();
        else if (speed < 0 && facingRight)
            FlipOrientation();
    }

    // Base function to move this pawn via transform positioning
    public virtual void Move(Vector3 direction)
    {
        transform.position += (direction * pawnSpeed * Time.deltaTime); // move our pawn in the provided direction

        // Determine if we need to flip their orientation
        if (direction.x < 0 && !facingRight)
            FlipOrientation();
        else if (direction.x > 0 && facingRight)
            FlipOrientation();
    }
    
    // Base function to make rigid body jump
    public virtual void Jump()
    {

    }

    // Function to orient our sprite left or right
    public void FlipOrientation()
    {
        facingRight = !facingRight;
        sr.flipX = !facingRight;
    }

    public virtual void Die()
    {
        alive = false; // set our alive bool
        // disable all of our colliders
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = false;
        }
        // set our pawn on an upward trajectory
        rb2d.velocity = Vector2.zero;
        rb2d.gravityScale = 1; // Set gravity scale in case this is normally a flying pawn
        rb2d.AddForce(new Vector2(0, deathForce));
        Destroy(this.gameObject, deathTime); // Destroy our pawn after the appropriate amount of time
    }
}
